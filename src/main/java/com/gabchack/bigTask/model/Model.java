package com.gabchack.bigTask.model;

import java.util.List;
import java.util.Set;

public interface Model {
    String getTEXT();

    List<String> getSENTENCES();

    List<String> getWORDS();

    //---------------------------------------------------------------------------------------------------1
    List<String> findSentencesWitchHaveWordDuplicates(List<String> sentences);

    //---------------------------------------------------------------------------------------------------2
    List<String> ascendingOrderAndNumberOfWords(List<String> sentences);

    //---------------------------------------------------------------------------------------------------3
    String getUniqueWordFromFirstSentence(List<String> sentences);

    //---------------------------------------------------------------------------------------------------4
    List<String> findAllWordsInInterrogativeSentences(List<String> sentences, int wordSize);

    //---------------------------------------------------------------------------------------------------5
    List<String> replaceFirstWordVowelLetterToLongest(List<String> sentences);

    //---------------------------------------------------------------------------------------------------6
    List<String> sortInAlphabeticalOrderByFirstChartOfWord(List<String> words);

    //---------------------------------------------------------------------------------------------------7
    List<String> sortWordsOfTextByVowelsPerCent(List<String> words);

    //---------------------------------------------------------------------------------------------------8
    List<String> sortWordsOfTextStartedWithVowelByNextConsonantLetter(List<String> words);

    //---------------------------------------------------------------------------------------------------9
    List<String> sortBySpecifiedLetterCount(List<String> words, Character letter);

    //---------------------------------------------------------------------------------------------------10
    List<String> countAndSortSelectedWords(List<String> words, Set<String> selectedWords);

    //---------------------------------------------------------------------------------------------------11
    List<String> removeSublineBetweenWords(List<String> sentences, String selectedWord);

    //---------------------------------------------------------------------------------------------------12
    List<String> removeWordsWithSelectedSizeStartedWithConsonant(List<String> sentences, int wordSize);

    //---------------------------------------------------------------------------------------------------13
    List<String> sortBySpecifiedLetterCountReverse(List<String> words, Character selectedChar);

    //---------------------------------------------------------------------------------------------------14
    String findBiggestPalindrome(String text);

    //---------------------------------------------------------------------------------------------------15
    List<String> transformWordsWithSelectedSize(List<String> sentences, int wordSize);

    //---------------------------------------------------------------------------------------------------16
    List<String> replaceWordsWithSize(List<String> sentences, int wordSize, String replacement);

    double countVowelsPercentageInTheWord(String word);
}
