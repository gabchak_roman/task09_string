package com.gabchack.bigTask.model.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class FileReader {

    private StringBuilder text = new StringBuilder();

    public FileReader(File file) throws FileNotFoundException {
        if (file.canRead()) {
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                text.append(reader.nextLine()).append(" ");
            }
        }
    }
    public String getStringText(){
        return text.toString();
    }
}
