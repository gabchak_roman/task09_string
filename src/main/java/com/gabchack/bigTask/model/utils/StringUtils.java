package com.gabchack.bigTask.model.utils;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


public class StringUtils {

    public static List<String> toSentencesList(String text) {
        List<String> sentences = new ArrayList<>();
        BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
        iterator.setText(text);
        int start = iterator.first();
        for (int end = iterator.next();
             end != BreakIterator.DONE;
             start = end, end = iterator.next()) {
            sentences.add(text.substring(start,end));
        }
        return sentences;
    }

    public static List<String> toWordsList(String text) {
        List<String> local = Arrays.stream(
                text.split("[.,\\d '’()?:-]"))
                .collect(Collectors.toList());
        local.removeAll(Arrays.asList("", null));
        return local;
    }

}
