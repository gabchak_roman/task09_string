package com.gabchack.bigTask.model;

import com.gabchack.bigTask.model.utils.FileReader;
import com.gabchack.bigTask.model.utils.StringUtils;

import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.SPACE;

public class ModelImpl implements Model{

    private final char[] VOWEL_LETTERS = new char[]{
            'a', 'e', 'i', 'o', 'u',
            'A', 'I', 'E', 'O', 'U',
            'а', 'е', 'и', 'і', 'о', 'у', 'я', 'ю', 'є', 'ї',
            'А', 'Е', 'И', 'І', 'О', 'У', 'Я', 'Ю', 'Є', 'Ї'};

    private final char[] DIGITS =
            new char[] {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    private final String TEXT;
    private final List<String> SENTENCES;
    private final List<String> WORDS;

    public ModelImpl(File textPath) throws FileNotFoundException {
        FileReader fileReader = new FileReader(textPath);
        this.TEXT = fileReader.getStringText();
        this.SENTENCES = StringUtils.toSentencesList(TEXT);
        this.WORDS = StringUtils.toWordsList(TEXT);
    }

    @Override
    public String getTEXT() {
        return TEXT;
    }

    @Override
    public List<String> getSENTENCES() {
        return SENTENCES;
    }

    @Override
    public List<String> getWORDS() {
        return WORDS;
    }


    //---------------------------------------------------------------------------------------------------1
    @Override
    public List<String> findSentencesWitchHaveWordDuplicates(List<String> sentences) {
        List<String> sentencesListWitchHaveWordDuplicates = new ArrayList<>();
        for (String sentence : sentences) {
            List<String> words = StringUtils.toWordsList(sentence);
            boolean check = false;
            for (int i = 0; i < words.size() - 1 && !check; i++) {
                for (int j = i + 1; j < words.size(); j++) {
                    if (words.get(i).equals(words.get(j))) {
                        check = true;
                        sentencesListWitchHaveWordDuplicates.add(
                                words.get(j) + " : "
                                        + sentence.replaceAll("\n", " "));
                        break;
                    }
                }
            }
        }
        return sentencesListWitchHaveWordDuplicates;
    }

    //---------------------------------------------------------------------------------------------------2
    @Override
    public List<String> ascendingOrderAndNumberOfWords(List<String> sentences) {
        return sentences.stream()
                .sorted(Comparator.comparingInt(String::length))
                .collect(Collectors.toList());
    }

    //---------------------------------------------------------------------------------------------------3
    @Override
    public String getUniqueWordFromFirstSentence(List<String> sentences) {
        if (sentences.size() >= 2) {
            List<String> firstSentenceWords = StringUtils.toWordsList(sentences.get(0));
            StringBuilder restOfTheText = new StringBuilder();
            for (int i = 1; i < sentences.size(); i++) {
                restOfTheText.append(sentences.get(i)).append(" ");
            }
            String text = restOfTheText.toString();
            for (String uWord : firstSentenceWords) {
                if (!text.contains(uWord)) {
                    return uWord;
                }
            }
        }
        return "------ Such word have not been found ------";
    }

    //---------------------------------------------------------------------------------------------------4
    @Override
    public List<String> findAllWordsInInterrogativeSentences(List<String> sentences, int wordSize) {
        Set<String> words = new HashSet<>();
        for (String sentence : sentences) {
            if (sentence.contains("?")) {
                for (String sentenceWord : StringUtils.toWordsList(sentence)) {
                    if (sentenceWord.length() == wordSize) {
                        words.add(sentenceWord);
                    }
                }
            }
        }
        return new ArrayList<>(words);
    }

    //---------------------------------------------------------------------------------------------------5
    @Override
    public List<String> replaceFirstWordVowelLetterToLongest(List<String> sentences) {
        List<String> sentencesLocal = new LinkedList<>(sentences);
        for (int i = 0; i < sentencesLocal.size(); i++) {
            if (sentencesLocal.get(i).length() > 0) {
                for (char vowelLetter : VOWEL_LETTERS) {
                    if (vowelLetter == sentencesLocal.get(i).charAt(0)) {
                        sentencesLocal.set
                                (i, replaceFirstWordToLongest(sentencesLocal.get(i)));
                        break;
                    }
                }
            }
        }
        return sentencesLocal;
    }

    private String replaceFirstWordToLongest(String sentence) {
        String[] words = StringUtils.toWordsList(sentence).toArray(new String[0]);
        int index = 0;
        int maxLength = words[0].length();
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() > maxLength) {
                maxLength = words[i].length();
                index = i;
            }
        }
        String temp = words[0];
        words[0] = words[index];
        words[index] = temp;
        return String.join(" ", words);
    }

    //---------------------------------------------------------------------------------------------------6
    @Override
    public List<String> sortInAlphabeticalOrderByFirstChartOfWord(List<String> words) {
        words.sort(Comparator.comparingInt(o -> o.charAt(0)));
        return words;
    }

    //---------------------------------------------------------------------------------------------------7
    @Override
    public List<String> sortWordsOfTextByVowelsPerCent(List<String> words) {
        words.sort(
                Comparator.comparingDouble(
                        this::countVowelsPercentageInTheWord));
        return words;
    }

    //---------------------------------------------------------------------------------------------------8
    @Override
    public List<String> sortWordsOfTextStartedWithVowelByNextConsonantLetter(
            List<String> words) {
        return words.stream().filter(this::startsWithVowel).sorted(Comparator
                .comparingInt(this::getFirstConsonantInt))
                .collect(Collectors.toList());
    }

    //---------------------------------------------------------------------------------------------------9
    @Override
    public List<String> sortBySpecifiedLetterCount(List<String> words, Character letter){
        Comparator<String> comparator =
                Comparator.comparingInt(w -> countOfLetter(w, letter));
        comparator = comparator.thenComparing(Function.identity());
        words.sort(comparator);
        return words;
    }

    //---------------------------------------------------------------------------------------------------10
    @Override
    public List<String> countAndSortSelectedWords(List<String> words,
            Set<String> selectedWords) {
        List<Pair<String, Long>> wordsCount = new ArrayList<>();
        for (String selectedWord : selectedWords) {
            wordsCount
                    .add(Pair.of(selectedWord, wordCount(words, selectedWord)));
        }
        wordsCount.sort(Comparator.comparing(Pair::getRight));
        return wordsCount.stream().map(p -> p.getLeft() + " - " + p.getRight())
                .collect(Collectors.toList());
    }

    //---------------------------------------------------------------------------------------------------11
    @Override
    public List<String> removeSublineBetweenWords(List<String> sentences, String selectedWord){
        int selectedWordLength = selectedWord.length();
        List<String> result = new ArrayList<>();
        for (String sentence : sentences) {
            sentence = sentence.toLowerCase();
            int startIndex = sentence.indexOf(selectedWord);
            if (startIndex >= 0) {
                int endIndex = sentence.lastIndexOf(selectedWord);
                if (endIndex > startIndex) {
                    result.add(sentence.substring(0, startIndex) + sentence
                            .substring(endIndex + selectedWordLength));
                    continue;
                }
            }
            result.add(sentence);
        }
        return result;
    }

    //---------------------------------------------------------------------------------------------------12
    @Override
    public List<String> removeWordsWithSelectedSizeStartedWithConsonant(
            List<String> sentences, int wordSize) {
        List<String> result = new ArrayList<>();
        for (String sentence : sentences) {
            sentence = sentence.toLowerCase();
            List<String> words = StringUtils.toWordsList(sentence);
            String newSentence = words.stream()
                    .filter(w -> !startsWithConsonant(w) || w.length() != wordSize)
                    .collect(Collectors.joining(SPACE));
            result.add(newSentence);
        }
        return result;
    }

    //---------------------------------------------------------------------------------------------------13
    @Override
    public List<String> sortBySpecifiedLetterCountReverse(List<String> words, Character letter) {
        Comparator<String> comparator =
                Comparator.comparingInt(w -> countOfLetter(w, letter));
        comparator = comparator.reversed();
        comparator = comparator.thenComparing(Function.identity());
        words.sort(comparator);
        return words;
    }

    //---------------------------------------------------------------------------------------------------14
    @Override
    public String findBiggestPalindrome(String text) {
        String biggestPalindrome = "";
        for (int i = 0; i < text.length(); i++) {
            for (int j = i + 1; j <= text.length(); j++) {
                String candidat = text.substring(i, j);
                if (isPalindrome(candidat) && candidat.length() > biggestPalindrome.length()) {
                    biggestPalindrome = candidat;
                }
            }
        }
        return biggestPalindrome;
    }

    //---------------------------------------------------------------------------------------------------15
    @Override
    public List<String> transformWordsWithSelectedSize(List<String> sentences, int wordSize) {
        List<String> result = new ArrayList<>();
        for (String sentence : sentences) {
            sentence = sentence.toLowerCase();
            List<String> words = StringUtils.toWordsList(sentence);
            result.add(words.stream().map(this::removeFirstAndLastLetterFromMiddle).collect(
                    Collectors.joining(SPACE)));
        }
        return result;
    }

    //---------------------------------------------------------------------------------------------------16
    @Override
    public List<String> replaceWordsWithSize(List<String> sentences, int wordSize, String replacement) {
        List<String> result = new ArrayList<>();
        for (String sentence : sentences) {
            sentence = sentence.toLowerCase();
            List<String> words = StringUtils.toWordsList(sentence);
            result.add(words.stream().map(w -> {
                if (w.length() == wordSize) {
                    return replacement;
                }
                return w;
            }).collect(Collectors.joining(SPACE)));
        }
        return result;
    }

    private String removeFirstAndLastLetterFromMiddle(String word) {
        if (word.length() <= 1) {
            return word;
        }
        char[] wordChars = word.toCharArray();
        StringBuilder builder = new StringBuilder();
        char first = wordChars[0];
        char last = wordChars[wordChars.length - 1];

        builder.append(first);
        for (int i = 1; i < wordChars.length - 1; i++) {
            char letter = wordChars[i];
            if (letter != first && letter != last) {
                builder.append(letter);
            }
        }
        builder.append(last);
        return builder.toString();
    }

    private boolean isPalindrome(String input) {
        StringBuilder plain = new StringBuilder(input);
        StringBuilder reverse = plain.reverse();
        return (reverse.toString()).equals(input);
    }

    private Long wordCount(List<String> words, String selectedWord) {
        return words.stream().filter(selectedWord::equals).count();
    }

    private int countOfLetter(String word, Character letter) {
        char[] wordChars = word.toCharArray();
        int count = 0;
        for (char wordChar : wordChars) {
            if (letter == wordChar) {
                count++;
            }
        }
        return count;
    }


    private int getFirstConsonantInt(String word) {
        char[] wordChars = word.toCharArray();

        for (char wordChar : wordChars) {
            if (isConsonant(wordChar)) {
                return (int) wordChar;
            }
        }
        return Integer.MAX_VALUE;
    }

    private boolean isConsonant(char c) {
        return !isVowel(c) && !isDigit(c);
    }

    private boolean startsWithConsonant(String word) {
        return word.length() > 0 && isConsonant(word.charAt(0));
    }

    private boolean startsWithVowel(String word) {
        return word.length() > 0 && isVowel(word.charAt(0));
    }

    private boolean isVowel(char c) {
        for (char vowelChar : VOWEL_LETTERS) {
            if (c == vowelChar) {
                return true;
            }
        }
        return false;
    }

    private boolean isDigit(char c) {
        for (char digitChar : DIGITS) {
            if (c == digitChar) {
                return true;
            }
        }
        return false;
    }

    @Override
    public double countVowelsPercentageInTheWord(String word) {
        double counter = 0;
        char[] wordChars = word.toCharArray();
        for (char wordChar : wordChars) {
            for (char vowelChar : VOWEL_LETTERS) {
                if (wordChar == vowelChar) {
                    counter++;
                }
            }
        }
        if (counter == 0 || word.length() == 0) {
            return 0;
        }
        return ((counter / (double) word.length()) * 100);
    }
    //---------------------------------------------------------------------------------------------------

}
