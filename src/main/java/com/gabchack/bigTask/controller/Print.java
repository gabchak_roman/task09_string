package com.gabchack.bigTask.controller;

@FunctionalInterface
public interface Print {
    void print();
}
