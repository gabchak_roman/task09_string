package com.gabchack.bigTask.controller;

import com.gabchack.bigTask.model.Model;
import com.gabchack.bigTask.view.View;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class ControllerImpl implements Controller{

    private static final String ENTER_A_WORD_SIZE = "Enter a word size:";
    private static final String ENTER_A_WORD = "Enter a word:";
    private static final String ENTER_A_LETTER = "Enter a letter:";
    private final int MENU_LINE_LENGTH = 76;
    private final int MENU_SUBSTRING = 12;
    private Scanner input = new Scanner(System.in);
    private Model model;
    private View view;
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public ControllerImpl(Model modelIn, View viewIn) {
        this.model = modelIn;
        this.view = viewIn;
    }

    @Override
    public void start() {
        view.printMenuMessage("-".repeat(MENU_LINE_LENGTH -11)
                + " Текстова утиліта "
                + "-".repeat(MENU_LINE_LENGTH -7));
        menu();
        print();
    }

    private void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", "|    1   -  Вивести найбільшу кількість речень тексту, в яких є однакові слова.                                                                        |");
        menu.put("2", "|    2   -  Вивести всі речення заданого тексту у порядку зростання кількості слів у кожному з них.                                                    |");
        menu.put("3", "|    3   -  Вивести таке слово у першому реченні, якого немає ні в одному з інших речень.                                                              |");
        menu.put("4", "|    4   -  У всіх запитальних реченнях тексту знайти і надрукувати без повторів слова заданої довжини.                                                |");
        menu.put("5", "|    5   -  У кожному реченні поміняти місцями перше слово, що починається на голосну букву з найдовшим словом. Вивести.                               |");
        menu.put("6", "|    6   -  Вивести слова тексту в алфавітному порядку по першій букві. Слова, що починаються з нової букви, друкувати з абзацного відступу.           |");
        menu.put("7", "|    7   -  Відсортувати слова тексту за зростанням відсотку голосних букв співвідношення кількості голосних до загальної кількості букв у слові)      |");
        menu.put("8", "|    8   -  Вивести слова тексту, що починаються з голосних букв, відсортувати в алфавітному порядку по першій приголосній букві слова.                |");
        menu.put("9", "|    9   -  Всі слова тексту відсортувати за зростанням кількості заданої букви у слові. Слова з однаковою кількістю розмістити у алфавітному порядку. |");
        menu.put("10", "|    10  -  Вивести для кожного слова з заданого списку знайти, скільки разів воно зустрічається у кожному реченні, і відсортувати                     |\n"
                     + "|           слова за спаданням загальної кількості входжень.                                                                                           |");
        menu.put("11", "|    11  -  У кожному реченні тексту видалити підрядок максимальної довжини, що починається і закінчується заданими символами.                         |");
        menu.put("12", "|    12  -  З тексту видалити всі слова заданої довжини, що починаються на приголосну букву.                                                           |");
        menu.put("13", "|    13  -  Відсортувати слова у тексті за спаданням кількості входжень заданого символу, а у випадку рівності – за алфавітом.                         |");
        menu.put("14", "|    14  -  У заданому тексті знайти підрядок максимальної довжини, який є паліндромом, тобто, читається зліва на право і справа на ліво однаково.     |");
        menu.put("15", "|    15  -  Перетворити кожне слово у тексті, видаливши з нього всі наступні (попередні) входження першої (останньої) букви цього слова.               |");
        menu.put("16", "|    16  -  У певному реченні тексту слова заданої довжини замінити вказаним підрядком, довжина якого може не співпадати з довжиною слова.             |");
        menu.put("Q",  "|    Q   -  Вийти з програми.                                                                                                                          |");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
        methodsMenu.put("12", this::pressButton12);
        methodsMenu.put("13", this::pressButton13);
        methodsMenu.put("14", this::pressButton14);
        methodsMenu.put("15", this::pressButton15);
        methodsMenu.put("16", this::pressButton16);
    }

    private void pressButton1() {
        view.printMenuMessage(menu.get("1").substring(MENU_SUBSTRING));
        view.printList(model.findSentencesWitchHaveWordDuplicates(model.getSENTENCES()));
    }

    private void pressButton2() {
        view.printMenuMessage(menu.get("2").substring(MENU_SUBSTRING));
        view.printList(model.ascendingOrderAndNumberOfWords(model.getSENTENCES()));
    }

    private void pressButton3() {
        view.printMenuMessage(menu.get("3").substring(MENU_SUBSTRING));
        view.printMessage(model.getUniqueWordFromFirstSentence(model.getSENTENCES()));
    }

    private void pressButton4() {
        view.printMenuMessage(menu.get("4").substring(MENU_SUBSTRING));
        int wordSize = readInt();
        view.printList(
                model.findAllWordsInInterrogativeSentences(model.getSENTENCES(),
                        wordSize));
    }

    private void pressButton5() {
        view.printMenuMessage(menu.get("5").substring(MENU_SUBSTRING));
        view.printList(model.replaceFirstWordVowelLetterToLongest(model.getSENTENCES()));
    }

    private void pressButton6() {
        view.printMenuMessage(menu.get("6").substring(MENU_SUBSTRING));
        view.printList(model.sortInAlphabeticalOrderByFirstChartOfWord(model.getWORDS()));
    }

    private void pressButton7() {
        view.printMenuMessage(menu.get("7").substring(MENU_SUBSTRING));
        view.printList(model.sortWordsOfTextByVowelsPerCent(model.getWORDS()));
    }

    private void pressButton8() {
        view.printMenuMessage(menu.get("8").substring(MENU_SUBSTRING));
        view.printList(model.sortWordsOfTextStartedWithVowelByNextConsonantLetter(model.getWORDS()));
    }

    private void pressButton9() {
        view.printMenuMessage(menu.get("9").substring(MENU_SUBSTRING));
        Character selectedChar = readLetter();
        view.printList(model.sortBySpecifiedLetterCount(model.getWORDS(), selectedChar));
    }

    private void pressButton10() {
        view.printMenuMessage(menu.get("10").substring(MENU_SUBSTRING));
        Set<String> selectedWords = readList();
        view.printList(model.countAndSortSelectedWords(model.getWORDS(), selectedWords));
    }

    private void pressButton11() {
        view.printMenuMessage(menu.get("11").substring(MENU_SUBSTRING));
        String selectedWord = readWord();
        view.printList(model.removeSublineBetweenWords(model.getSENTENCES(), selectedWord));
    }
    private void pressButton12() {
        view.printMenuMessage(menu.get("12").substring(MENU_SUBSTRING));
        int wordSize = readInt();
        view.printList(model.removeWordsWithSelectedSizeStartedWithConsonant(
                model.getSENTENCES(), wordSize));
    }
    private void pressButton13() {
        view.printMenuMessage(menu.get("13").substring(MENU_SUBSTRING));
        Character selectedChar = readLetter();
        view.printList(model.sortBySpecifiedLetterCountReverse(model.getWORDS(), selectedChar));
    }
    private void pressButton14() {
        view.printMenuMessage(menu.get("14").substring(MENU_SUBSTRING));
        view.printMenuMessage(model.findBiggestPalindrome(model.getTEXT()));
    }
    private void pressButton15() {
        view.printMenuMessage(menu.get("15").substring(MENU_SUBSTRING));
        int wordSize = readInt();
        view.printList(model.transformWordsWithSelectedSize(model.getSENTENCES(), wordSize));
    }
    private void pressButton16() {
        view.printMenuMessage(menu.get("16").substring(MENU_SUBSTRING));
        int wordSize = readInt();
        String replacement = readWord();
        view.printList(model.replaceWordsWithSize(model.getSENTENCES(), wordSize, replacement));
    }

    private Character readLetter() {
        Character selectedChar = null;
        view.printMenuMessage(ControllerImpl.ENTER_A_LETTER);
        try {
            selectedChar = input.nextLine().charAt(0);
        } catch (Exception e) {
            view.printMessage("Bad character");
        }
        return selectedChar;
    }

    private String readWord() {
        view.printMenuMessage(ControllerImpl.ENTER_A_WORD);
        String word =  null;
        try {
            word = input.nextLine();
        } catch (Exception e) {
            view.printMessage("Bad word");
        }
        return word;
    }

    private int readInt() {
        int val = 0;
        view.printMenuMessage(ControllerImpl.ENTER_A_WORD_SIZE);
        try {
            val = input.nextInt();
            input.nextLine();
        } catch (Exception e) {
            view.printMessage("This is not a number");
        }
        return val;
    }

    private Set<String> readList() {
        Set<String> selectedWords = new HashSet<>();
        view.printMenuMessage("Enter comma separated word list:");
        try {
            String[] parts = input.nextLine().split(",");
            for (String part : parts) {
                selectedWords.add(part.trim());
            }
        } catch (Exception e) {
            view.printMessage("Bad character");
        }
        return selectedWords;
    }

    private void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMenuMessage("▬▬".repeat(MENU_LINE_LENGTH));
            view.printMenuMessage("Please, select menu point.  ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMenuMessage("►◄".repeat(MENU_LINE_LENGTH));
        view.printMenuMessage(" ".repeat(MENU_LINE_LENGTH -5) + "МЕНЮ:");
        view.printMenuMessage("►◄".repeat(MENU_LINE_LENGTH));
        menu.values().forEach(
                message -> view.printMenuMessage(message));
    }
}

