package com.gabchack.bigTask;

import com.gabchack.bigTask.controller.Controller;
import com.gabchack.bigTask.controller.ControllerImpl;
import com.gabchack.bigTask.model.Model;
import com.gabchack.bigTask.model.ModelImpl;
import com.gabchack.bigTask.view.View;
import com.gabchack.bigTask.view.ViewImpl;

import java.io.File;
import java.io.FileNotFoundException;

 public class Application {
     public static void main(String[] args) throws FileNotFoundException {

        File textPath = new File("text");
        View view = new ViewImpl();
        Model model = new ModelImpl(textPath);
        Controller controller = new ControllerImpl(model, view);
        controller.start();

    }
}