package com.gabchack.bigTask.view;

import java.util.List;

public interface View {

    void printMenuMessage(String message);

    void printMessage(String message);

    void printList(List<String> list);
}
