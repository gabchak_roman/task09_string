package com.gabchack.bigTask.view;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ViewImpl implements View{

    private static final String ANSI_CYAN   = "\u001B[36m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public void printMenuMessage(String message) {
        System.out.println(ANSI_CYAN + message + ANSI_RESET);
    }

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void printList(List<String> list) {
        AtomicInteger counter = new AtomicInteger();
        list.forEach(s -> System.out.println(counter.incrementAndGet() + " : " + s));
    }
}
