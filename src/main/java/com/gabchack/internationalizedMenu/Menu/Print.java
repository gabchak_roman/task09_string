package com.gabchack.internationalizedMenu.Menu;

@FunctionalInterface
public interface Print {
    void print();
}
