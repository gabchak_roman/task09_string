package com.gabchack.internationalizedMenu.Menu;

import com.gabchack.bigTask.controller.Print;
import com.gabchack.bigTask.model.utils.StringUtils;

import java.util.*;

public class Menu implements com.gabchack.internationalizedMenu.Menu.Print {

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    private String text;
    private ResourceBundle bundle;
    private Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public Menu() {
        this.bundle = ResourceBundle.getBundle("UtilMenu");
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("11", this::initializeMenuUkrainian);
        methodsMenu.put("22", this::initializeMenuEnglish);
        methodsMenu.put("33", this::initializeMenuChina);
        methodsMenu.put("44", this::initializeMenuKorea);
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("Menu", bundle.getString("Menu"));
        menu.put("Line", bundle.getString("Line"));
        menu.put("0", bundle.getString("0"));
        menu.put("11", bundle.getString("11"));
        menu.put("22", bundle.getString("22"));
        menu.put("33", bundle.getString("33"));
        menu.put("44", bundle.getString("44"));
        menu.put("Line2", bundle.getString("Line2"));
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("Q", bundle.getString("Q"));
        menu.put("Line3", bundle.getString("Line3"));
        menu.put("Point", bundle.getString("Point"));
    }

    public void start(String text) {
        this.text = text;
        setMenu();
        print();
    }

    private void initializeMenuUkrainian() {
        this.bundle = ResourceBundle.getBundle("UtilMenu_ua");
        setMenu();
    }

    private void initializeMenuEnglish() {
        this.bundle = ResourceBundle.getBundle("UtilMenu");
        setMenu();
    }

    private void initializeMenuChina() {
        this.bundle = ResourceBundle.getBundle("UtilMenu_cn");
        setMenu();
    }

    private void initializeMenuKorea() {
        this.bundle = ResourceBundle.getBundle("UtilMenu_kr");
        setMenu();
    }

    private void pressButton1() {
        printText(text);
    }

    private void pressButton2() {
        List<String> words = StringUtils.
                toWordsList(text.replaceAll("\n", ""));
        words.sort(Comparator.comparingInt(String::length));
        words.forEach(this::printText);
    }

    private void printText(String text) {
        System.out.println(ANSI_YELLOW + text + ANSI_RESET);
    }

    public void print() {
        String keyMenu;
        do {
            outputMenu();
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        menu.values().forEach(System.out::println);
    }
}


