package com.gabchack.internationalizedMenu;

import com.gabchack.internationalizedMenu.Menu.Menu;

public class Main {
    public static void main(String[] args) {
        String text = "Буває, часом сліпну від краси.\n" +
                "Спинюсь, не тямлю, що воно за диво,–\n" +
                "оці степи, це небо, ці ліси,\n" +
                "усе так гарно, чисто, незрадливо,\n" +
                "усе як є – дорога, явори,\n" +
                "усе моє, все зветься – Україна.\n" +
                "Така краса, висока і нетлінна,\n" +
                "що хоч спинись і з Богом говори.";
        Menu menu = new Menu();
        menu.start(text);
    }
}
